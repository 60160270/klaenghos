from django.contrib import admin
from .models import Schedule
# Register your models here.

class ScheduleAdmin(admin.ModelAdmin):
    list_display = ("name","upload_file")

admin.site.register(Schedule, ScheduleAdmin)