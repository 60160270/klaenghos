from django.db import models

# Create your models here.


class Schedule(models.Model):
    name = models.CharField(max_length=100)
    upload_file = models.FileField(default="")

    def _str_(self):
        return self.name
